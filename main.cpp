#include "OutStream.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"
#define AGE 1500
int main(int argc, char **argv)
{
	OutStream screen;
	screen << "I am the Doctor and I'm "<< AGE <<" years old" << endline;
	FileStream file("file.txt");
	file << "I am the Doctor and I'm " << AGE << " years old" << endline;
	OutStreamEncrypted encrypted = OutStreamEncrypted(1);
	encrypted << "I am the Doctor and I'm " << AGE << " years old" << endline;
	Logger file1("file1.txt", false);
	file1.print("hallo");
	Logger file2("file2.txt", true);
	file2.print("hi");
	return 0;
}
